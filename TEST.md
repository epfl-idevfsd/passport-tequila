# Prerequisites

Make sure [OpenSSL](https://www.openssl.org/) is available on your machine.

On Windows, add its binary directory to the [PATH variable](https://www.computerhope.com/issues/ch000549.htm)
