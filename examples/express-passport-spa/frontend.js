const express = require('express'),
    https = require('https'),
    ca = require('../lib/ca'),
    fs = require('fs');

const frontendPort = 3000;

const app = express();

// This is how you Tequila-protect a page:
app.get('/', function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(fs.readFileSync(__dirname + '/index.html'));
    res.end();
});

// Get a SSL certificate...
ca().then(({ ca, cached }) => {
  // And create the server.
  const httpsServer = https.createServer(ca, app);
  // Then output guidelines.
  httpsServer.listen(frontendPort, () => {
    const url = `\x1b[32mhttps://localhost:${httpsServer.address().port}\x1b[0m`;
    console.log(`Frontend server listening on: ${url}`);
    if (!cached) {
      console.log('Your browser may warn you the connection is not private.');
      console.log('It\'s because the SSL certificate is self-signed.');
      console.log('Just proceed to the website anyway.');
    }
  });
});
