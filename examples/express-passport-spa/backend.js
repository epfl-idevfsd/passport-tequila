const debug = require('debug'),
    express = require('express'),
    passport = require('passport'),
    https = require('https'),
    ca = require('../lib/ca'),
    TequilaServerSideFlow = require('../../lib/passport-tequila').ServerSideFlow,
    TequilaProtocol = require('../../lib/passport-tequila').Protocol,
    cors = require('cors');

const backendPort = 5300;

const flow = new TequilaServerSideFlow({
    service: "Demo Tequila back-end in node.js",
    request: ["displayname"],
    redirectUrl: "https://localhost:3000"
})

const app = express();
app.use(cors());

// Your Web app's frontend shall call this endpoint to obtain
// credentials. (In this example the token is always the same, and it
// is unused to boot; in a real app you would obviously want an
// unguessable token that both client and server remember somehow)
app.get('/login', async function(req, res) {
  try {
    if (req.query && req.query.url) {
      const identity = await flow.validateTequilaReturn(req.query.url);
      console.log("Tequila-side identity is ", identity);
      res.json({ok: true, token: 12345})
    } else {
        res.json({ok: true, redirect: await flow.prepareLogin()})
    }
  } catch (e) {
    res.status(500);
    res.header("Content-Type", "text/json");
    res.send(JSON.stringify({ ok: false, error: String(e)}))
  }
});

// To log out, just drop the session cookie.
app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

// Get a SSL certificate...
ca().then(({ ca, cached }) => {
  // And create the server.
  const httpsServer = https.createServer(ca, app);
  // Then output guidelines.
  httpsServer.listen(backendPort, () => {
    const url = `\x1b[32mhttps://localhost:${httpsServer.address().port}\x1b[0m`;
    console.log(`Backend server listening on: ${url}`);
    if (!cached) {
      console.log('Your browser may warn you the connection is not private.');
      console.log('It\'s because the SSL certificate is self-signed.');
      console.log('Just proceed to the website anyway.');
    }
  });
});
