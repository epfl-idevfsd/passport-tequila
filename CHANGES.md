# Version 1.2.0
- Add the ability to customize the Tequila RPCs, via `tequila_createrequest_options` and `tequila_fetchattributes_options` attributes to `Protocol` instances

# Version 1.1.0
- Support for “Single-page app” (SPA) use-case, whence there is no session management at all, and no in-line (“middleware”) access control, done by passport-tequila at all

# Version 1.0.4
- Fix tests not working on all OS
- Switch example app to HTTPS (fixes error when getting back from Tequila)
- Add npm scripts for example app and test with console debug output

# Version 1.0.3
- Fix not letting the error(s) raise(s) when the HTTPSServer is created

# Version 1.0.2
- Moving gitlab organisation

# Version 1.0.1
- Fix fake server not being compatible with the latest version
- Set autofocus on the submit button for the fake UI

# Version 1.0.0
## Breaking change
- `protocol.fetchattributes` parameters signature goes from `(key, done)` to `(key, auth_key, done)`
- Add protocol 2.1 support

# Version 0.2.0

- Introduce configuration file for the `requestauth` form in the fake Tequila server
