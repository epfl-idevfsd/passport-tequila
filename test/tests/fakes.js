var expect = require('chai').expect,
    Q = require('q'),
    _ = require('underscore'),
    ip = require('ip');

var fakes = require('../fakes'),
    request = fakes.request;

describe("fakes.TequilaServer", function () {
    var server = new fakes.TequilaServer();
    before(function(done) {
        server.start(done);
    });

    after(function(done) {
        server.close(done);
    });

    it("serves", function (done) {
        Q.nfcall(request, 'https://localhost:' + server.port + "/404")
            .should.be.fulfilled.then(function(callbackArgs) {
            var res = callbackArgs[0];
            expect(res.statusCode).to.equal(404);
        }).should.notify(done);
    });

    var localIp = ip.address();
    it("serves on all interfaces", (! localIp) ? undefined : function (done) {
        Q.nfcall(request, 'https://' + localIp + ':' + server.port + "/404")
            .should.be.fulfilled.then(function (callbackArgs) {
            var res = callbackArgs[0];
            expect(res.statusCode).to.equal(404);
        }).should.notify(done);
    });
    it("serves on /cgi-bin/tequila/createrequest", function (done) {
        var host = "localhost:" + server.port;
        var url = "/cgi-bin/tequila/createrequest";
        var body = "urlaccess=http://myhost.mydomain/myapp\n" +
            "request=name,firstname\n" +
            "require=group=somegroup\n" +
            "allows=category=guest";
        Q.nfcall(request.post, { url: 'https://' + host + url, body })
            .should.be.fulfilled.then(function (callbackArgs) {
            var res = callbackArgs[0],
                resBody = callbackArgs[1];
            expect(res.statusCode).to.equal(200);
            var matched = String(resBody).match(/key=(.*)/);
            expect(matched).to.be.an('Array');
            expect(server.state[matched[1]]).to.deep.equal({
                url: url,
                method: 'POST',
                headers: {
                    'connection': 'close',
                    'content-length': String(body.length),
                    'content-type': 'text/plain',
                    'host': host,
                },
                teqParams: {
                    urlaccess: "http://myhost.mydomain/myapp",
                    request: "name,firstname",
                    require: "group=somegroup",
                    allows: "category=guest",
                },
            });
        }).should.notify(done);
    });
});
