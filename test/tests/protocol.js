var assert = require('assert'),
    chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    Q = require('q'),
    _ = require('underscore');

chai.use(chaiAsPromised);
chai.should();

var Protocol = require('../../lib/passport-tequila/protocol.js'),
    fakes = require('../fakes');

describe("protocol.js", function() {
    var server = new fakes.TequilaServer();
    before(function(done) {
        server.start(done);
    });

    after(function(done) {
        server.close(done);
    });

    it("createrequest", function(done) {
        var protocol = new Protocol();
        _.extend(protocol, server.getOptions());

        Q.ninvoke(protocol, "createrequest", "/")
            .should.be.fulfilled.then(function(tequilaResult) {
                assert(tequilaResult.key);
            })
            .should.notify(done);
    });
});
